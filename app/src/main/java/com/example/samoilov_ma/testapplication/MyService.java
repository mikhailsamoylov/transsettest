package com.example.samoilov_ma.testapplication;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

/**
 * Created by samoilov_ma on 05.08.2016.
 * Just a service which does some useless work
 */
public class MyService extends Service {

    /**
     * Logging tag
     */
    final String TAG = "MyService";

    /**
     * Just a thread for doing useless work
     */
    Thread t;

    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        someTask();
        return START_REDELIVER_INTENT;
    }

    public void onDestroy() {
        super.onDestroy();
        t.interrupt();
        Log.d(TAG, "onDestroy()");
    }

    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind()");
        return null;
    }

    /**
     * Method with useless rows of code
     */
    void someTask() {
        t = new Thread(new Runnable() {
            public void run() {
                for (int i = 1; i <= 25; i++) {
                    Log.d(TAG, "i = " + i);
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        Log.d(TAG, "someTask() was interrupted");
                        return;
                    }
                }
                stopSelf();
            }
        });
        t.start();
    }
}
