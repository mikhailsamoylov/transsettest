package com.example.samoilov_ma.testapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by samoilov_ma on 04.08.2016.
 * PremainActivity with 'Create Table' button
 */
public class PremainActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premain);

        final TestTable testTable = new TestTable(getApplicationContext());
        if (testTable.whetherTableExists()) {
            go2MainActivity();
        }

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                testTable.createTestTable();
                go2MainActivity();
            }
        });
    }

    /**
     * Opens MainActivity and finishes current one
     */
    private void go2MainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }
}
