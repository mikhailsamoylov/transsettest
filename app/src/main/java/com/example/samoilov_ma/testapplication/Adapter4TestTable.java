package com.example.samoilov_ma.testapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by samoilov_ma on 08.08.2016.
 * Adapter class especially for 'test' table list view
 */
public class Adapter4TestTable extends BaseAdapter {

    ArrayList<TestTable> testTable;
    LayoutInflater lInflater;

    Adapter4TestTable (Context context, ArrayList<TestTable> testTable) {
        this.testTable = testTable;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return testTable.size();
    }

    @Override
    public Object getItem(int i) {
        return testTable.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View customView = view;
        if (customView == null) {
            customView = lInflater.inflate(R.layout.list_item, viewGroup, false);
        }
        TestTable testTable = (TestTable)getItem(i);

        ((TextView) customView.findViewById(R.id.id)).setText(Long.toString(testTable.id));
        ((TextView) customView.findViewById(R.id.date)).setText(testTable.datetime);
        ((TextView) customView.findViewById(R.id.name)).setText(testTable.name);
        ((TextView) customView.findViewById(R.id.value)).setText(Double.toString(testTable.value));
        return customView;
    }
}
