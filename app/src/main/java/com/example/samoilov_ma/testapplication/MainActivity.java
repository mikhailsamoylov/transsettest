package com.example.samoilov_ma.testapplication;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /****************
     * UI elements
     ****************/
    private Button selectButton;
    private Button insertButton;
    private Button updateButton;
    private Button deleteButton;
    private Button addButton;
    private Button dropTableButton;
    private Button insideTransactionButton;
    private Button outsideTransactionButton;

    private EditText firstEditText;
    private EditText secondEditText;
    private ListView selectionListView;
    private ProgressDialog progressDialog;
    /**********************************************/

    /**
     * List with data to insert
     */
    private List<String> data2insertList = new ArrayList<>();

    /**
     * Logging tag
     */
    private static final String TAG = "MainActivity";

    /**
     * Handler for messages handling
     */
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");
        setContentView(R.layout.activity_main);

        firstEditText = (EditText) findViewById(R.id.firstEditText);
        secondEditText = (EditText) findViewById(R.id.secondEditText);

        selectButton = (Button) findViewById(R.id.selectButton);
        insertButton = (Button) findViewById(R.id.insertButton);
        updateButton = (Button) findViewById(R.id.updateButton);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        addButton = (Button) findViewById(R.id.addButton);
        dropTableButton = (Button) findViewById(R.id.dropTableButton);
        insideTransactionButton = (Button) findViewById(R.id.insideTransactionButton);
        outsideTransactionButton = (Button) findViewById(R.id.outsideTransactionButton);

        selectButton.setOnClickListener(this);
        insertButton.setOnClickListener(this);
        updateButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);
        addButton.setOnClickListener(this);
        dropTableButton.setOnClickListener(this);
        insideTransactionButton.setOnClickListener(this);
        outsideTransactionButton.setOnClickListener(this);

        selectionListView = (ListView) findViewById(R.id.selectionListView);

        handler = new Handler() {
            public void handleMessage(Message msg) {
                Toast.makeText(MainActivity.this, "Time: " + msg.what, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        };
    }

    @Override
    public void onClick(View view) {
        String pk;
        final TestTable testTable = new TestTable(getApplicationContext());
        switch (view.getId()) {

            case R.id.addButton:
                String str = firstEditText.getText().toString();
                if (!str.equals("")) {
                    data2insertList.add(str);
                } else {
                    Toast.makeText(MainActivity.this,
                            "Please, insert some value into first text field",
                            Toast.LENGTH_LONG).show();
                }
                clearFields();
                break;

            case R.id.insertButton:
                if (!data2insertList.isEmpty()) {
                    testTable.insertDataList(data2insertList);
                    data2insertList.clear();
                } else {
                    Toast.makeText(MainActivity.this,
                            "There is nothing to insert",
                            Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.selectButton:
                pk = firstEditText.getText().toString();
                ArrayList<TestTable> dataList = new ArrayList<>();

                if (pk.equals("")) {
                    dataList = testTable.getData();
                } else {
                    try {
                        dataList = testTable.getData(Integer.parseInt(pk));
                    } catch (NumberFormatException nfe) {
                        Toast.makeText(MainActivity.this, "Primary key should be an integer",
                                Toast.LENGTH_LONG).show();
                        Log.i(TAG, "Primary key should be an integer");
                    }
                }

                ListAdapter adapter = new Adapter4TestTable(getApplicationContext(), dataList);
                selectionListView.setAdapter(adapter);
                break;

            case R.id.updateButton:
                int id;
                double newValue;
                try {
                    id = Integer.parseInt(firstEditText.getText().toString());
                } catch (NumberFormatException nfe) {
                    Toast.makeText(MainActivity.this,
                            "Primary key should be an integer and should not be empty",
                            Toast.LENGTH_LONG).show();
                    Log.i(TAG, "Primary key should be an integer and should not be empty");
                    return;
                }

                try {
                    newValue = Double.parseDouble(secondEditText.getText().toString());
                } catch (NumberFormatException nfe) {
                    Toast.makeText(MainActivity.this, "New value should be numeric",
                            Toast.LENGTH_LONG).show();
                    Log.i(TAG, "New value should be numeric");
                    return;
                }

                testTable.updateValueByPk(id, newValue);
                break;

            case R.id.deleteButton:
                pk = firstEditText.getText().toString();

                if (pk.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("It cannot be undone")
                            .setTitle("Are you sure?")
                            .setPositiveButton("Yeah, I'm sure", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int arg1) {
                                    testTable.deleteData();
                                }
                            })
                            .setNegativeButton("No, please, no!", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int arg1) {
                                }
                            });

                    builder.create().show();
                } else {
                    try {
                        testTable.deleteData(Integer.parseInt(pk));
                    } catch (NumberFormatException nfe) {
                        Toast.makeText(MainActivity.this,
                                "Primary key should be an integer and should not be empty",
                                Toast.LENGTH_LONG).show();
                        Log.i(TAG, "Primary key should be an integer and should not be empty");
                    }
                }

                clearFields();
                break;

            case R.id.dropTableButton:
                testTable.dropTestTable();
                Intent intent = new Intent(getApplicationContext(), PremainActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.insideTransactionButton:
                makeProgressDialog();
                (new Thread(new Runnable() {
                    public void run() {
                        long t1 = new Date().getTime();
                        testTable.insertNRecordsInsideTransaction(3000);
                        long t2 = new Date().getTime();
                        handler.sendEmptyMessage((int) (t2 - t1));
                    }
                })).start();
                break;

            case R.id.outsideTransactionButton:
                makeProgressDialog();
                (new Thread(new Runnable() {
                    public void run() {
                        long t1 = new Date().getTime();
                        testTable.insertNRecordsOutsideTransaction(3000);
                        long t2 = new Date().getTime();
                        handler.sendEmptyMessage((int) (t2 - t1));
                    }
                })).start();
                break;
        }
    }

    /**
     * Clears text fields
     */
    private void clearFields() {
        firstEditText.getText().clear();
        secondEditText.getText().clear();
    }

    /**
     * Makes ProgressDialog
     */
    private void makeProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please, wait");
        progressDialog.setMessage("Query is in process");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    protected void onStart() {
        super.onStart();
        startService(new Intent(this, MyService.class));
        Log.d(TAG, "onStart()");
    }

    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart()");
    }

    protected void onResume() {
        super.onResume();
        ActivityManager am = (ActivityManager) this
                .getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> rs = am.getRunningServices(50);

        for (int i = 0; i < rs.size(); i++) {
            ActivityManager.RunningServiceInfo rsi = rs.get(i);
            Log.i("Service", "Process " + rsi.process + " with component "
                    + rsi.service.getClassName());
        }
        Log.d(TAG, "onResume()");
    }

    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }

    protected void onStop() {
        super.onStop();
        stopService(new Intent(this, MyService.class));
        Log.d(TAG, "onStop()");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, "onNewIntent()");
    }
}
